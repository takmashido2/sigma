package takmashido.channel;

import java.math.BigInteger;
import java.util.function.Consumer;

public interface Channel{
    public boolean connect(Consumer<BigInteger> receiver);
    public boolean disconnect(Consumer<BigInteger> receiver);

    public boolean send(int packet);
    public boolean send(BigInteger packet);
    public boolean send(BigInteger... packets);
}