package takmashido.channel;

import java.util.function.Consumer;
import java.util.List;
import java.lang.ref.WeakReference;
import java.math.BigInteger;
import java.util.ArrayList;

public class BasicChannel implements Channel{
    private List<WeakReference<Consumer<BigInteger>>> receivers=new ArrayList<>();

    @Override
    public synchronized boolean connect(Consumer<BigInteger> receiver){
        receivers.add(new WeakReference<Consumer<BigInteger>>(receiver));
        return true;
    }
    @Override
    public boolean disconnect(Consumer<BigInteger> receiver){
        for(int i=0;i<receivers.size();i++)
            if(receivers.get(i).refersTo(receiver)){
                receivers.remove(i);
                return true;
            }
        
        return false;
    }

    @Override
    public boolean send(int value){
        return send(BigInteger.valueOf(value));
    }
    @Override
    public boolean send(BigInteger value){
        List<WeakReference<Consumer<BigInteger>>> toDelete=new ArrayList<>(0);

        for(var receiver:receivers){
            Consumer<BigInteger> rec=receiver.get();
            if(rec!=null)
                rec.accept(value);
            else
                toDelete.add(receiver);
        }

        for(var del:toDelete)
            receivers.remove(del);

        return true;
    }
    @Override
    public boolean send(BigInteger... packets) {
        for(BigInteger packet:packets)
            if(!send(packet))
                return false;
        
        return true;
    }
}