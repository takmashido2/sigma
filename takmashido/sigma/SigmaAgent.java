package takmashido.sigma;

import java.io.Closeable;
import java.math.BigInteger;
import java.security.InvalidKeyException;
import java.security.KeyFactory;
import java.security.KeyPair;
import java.security.KeyPairGenerator;
import java.security.NoSuchAlgorithmException;
import java.security.PublicKey;
import java.security.SecureRandom;
import java.security.Signature;
import java.security.SignatureException;
import java.security.interfaces.RSAPublicKey;
import java.security.spec.InvalidKeySpecException;
import java.security.spec.KeySpec;
import java.security.spec.RSAPublicKeySpec;

import takmashido.channel.Channel;

import java.util.concurrent.BlockingQueue;
import java.util.concurrent.LinkedBlockingQueue;
import java.util.function.Consumer;

import javax.crypto.BadPaddingException;
import javax.crypto.Cipher;
import javax.crypto.IllegalBlockSizeException;
import javax.crypto.Mac;
import javax.crypto.NoSuchPaddingException;
import javax.crypto.SecretKey;
import javax.crypto.SecretKeyFactory;
import javax.crypto.spec.PBEKeySpec;
import javax.crypto.spec.SecretKeySpec;

public class SigmaAgent implements Closeable, Runnable{
    private Channel inChannel;
    private Channel outChannel;
    private KeyPair keyPair;

    private Consumer<BigInteger> receiver=this::receiver;       //Exist only to prevent GC from cleaning this::receiver weak reference in outChannel

    private BlockingQueue<BigInteger> input=new LinkedBlockingQueue<>();

    private PublicKey secondPartyPublicKey;
    private SecretKey transmissionKey;
    private SecretKey encryptionKey;
    private SecretKey macKey;

    private Cipher transmissionEncryptionCipher;
    private Cipher transmissionDecryptionCipher;
    private Cipher encryptionCipher;
    private Cipher decryptionCipher;

    private Mac mac;

    private boolean firstParty;

    public SigmaAgent(Channel inChannel, Channel outChannel, boolean firstParty) throws NoSuchAlgorithmException{
        this.inChannel=inChannel;
        inChannel.connect(receiver);

        this.outChannel=outChannel;

        this.firstParty=firstParty;

        KeyPairGenerator rsaGen=KeyPairGenerator.getInstance("RSA");
        rsaGen.initialize(2048);

        keyPair=rsaGen.genKeyPair();
    }

    private void receiver(BigInteger message){
        input.add(message);
    }

    @Override
    public void run(){
        try{
            if(firstParty){
                //<packet1><packet1><packet1><packet1><packet1><packet1>
                SecureRandom rand=new SecureRandom();
                BigInteger mod=new BigInteger(256, 50, rand);                    //Generate g and modulo
                BigInteger g=mod;
                while(g.compareTo(mod)>=0)
                    g=new BigInteger(256, 50, rand);                    //Generate g and modulo
                
                BigInteger x=new BigInteger(128, rand);
                
                BigInteger g_x=g.modPow(x, mod);

                outChannel.send(g);    //Send to B
                outChannel.send(mod);
                outChannel.send(g_x);
                System.out.println("send 1 packet");

                //<packet2><packet2><packet2><packet2><packet2><packet2>
                BigInteger g_y=input.take();

                BigInteger g_xy=g_y.modPow(x, mod);
                prepareTransmissionEncryption(g_xy);
                prepareMac(g_xy);
                prepareEncryption(g_xy);

                BigInteger[] packet=decrypt(transmissionDecryptionCipher,input.take());
                BigInteger b_modulus=packet[0];
                BigInteger b_exponent=packet[1];
                secondPartyPublicKey=KeyFactory.getInstance("RSA").generatePublic(new RSAPublicKeySpec(b_modulus, b_exponent));
                BigInteger sig_b=packet[2];
                BigInteger mac_k_m_b=packet[3];

                System.out.println("received 2 packet");

                //Signature verification
                Signature bSignature=Signature.getInstance("SHA256withRSA");
                bSignature.initVerify(secondPartyPublicKey);
                bSignature.update(CryptMath.toByteArr(g_x,g_y));
                if(!bSignature.verify(CryptMath.toByteArr(sig_b))){
                    System.out.println("Failed to verify signature of g_x, and g_y values");
                    return;
                }

                //Message mac verification
                if(!mac_k_m_b.equals(new BigInteger(mac.doFinal(
                        CryptMath.toByteArr(g_y, b_modulus, b_exponent, sig_b))))){
                    System.out.println("Failed to verify 2 packet mac");
                    return;
                }

                //<packet3><packet3><packet3><packet3><packet3><packet3>
                RSAPublicKey pub=(RSAPublicKey)keyPair.getPublic();
                Signature signature=Signature.getInstance("SHA256withRSA");
                signature.initSign(keyPair.getPrivate());

                BigInteger[] message=new BigInteger[3];
                message[0]=pub.getModulus();
                message[1]=pub.getPublicExponent();
                signature.update(CryptMath.toByteArr(g_y,g_x));
                message[2]=new BigInteger(1,signature.sign());

                BigInteger packetMac=new BigInteger(mac.doFinal(CryptMath.toByteArr(message)));

                outChannel.send(encrypt(transmissionEncryptionCipher,message[0], message[1], message[2], packetMac));

                System.out.println("send 3 packet");
            } else {
                SecureRandom rand=new SecureRandom();
                BigInteger y=new BigInteger(128, 50, rand);

                //<packet1><packet1><packet1><packet1><packet1><packet1>
                BigInteger g=input.take();
                BigInteger mod=input.take();
                BigInteger g_x=input.take();

                System.out.println("received 1 packet");

                //<packet2><packet2><packet2><packet2><packet2><packet2>
                BigInteger g_xy=g_x.modPow(y, mod);
                prepareTransmissionEncryption(g_xy);
                prepareMac(g_xy);
                prepareEncryption(g_xy);

                BigInteger g_y=g.modPow(y, mod);

                RSAPublicKey pub=(RSAPublicKey)keyPair.getPublic();
                Signature signature=Signature.getInstance("SHA256withRSA");
                signature.initSign(keyPair.getPrivate());

                BigInteger[] message=new BigInteger[4];
                message[0]=g_y;
                message[1]=pub.getModulus();
                message[2]=pub.getPublicExponent();
                signature.update(CryptMath.toByteArr(g_x,g_y));
                message[3]=new BigInteger(signature.sign());
                outChannel.send(message[0]);

                BigInteger packet_mac=new BigInteger(mac.doFinal(CryptMath.toByteArr(message)));

                outChannel.send(encrypt(transmissionEncryptionCipher, message[1],message[2],message[3],packet_mac));      //message[0] skipped -> secondParty needs this info to derive key

                System.out.println("send 2 packet");

                //<packet3><packet3><packet3><packet3><packet3><packet3>
                
                BigInteger[] packet=decrypt(transmissionDecryptionCipher,input.take());

                BigInteger a_modulus=packet[0];
                BigInteger a_exponent=packet[1];
                secondPartyPublicKey=KeyFactory.getInstance("RSA").generatePublic(new RSAPublicKeySpec(a_modulus, a_exponent));
                BigInteger sig_a=packet[2];
                BigInteger mac_k_m_a=packet[3];

                //Signature verification
                Signature bSignature=Signature.getInstance("SHA256withRSA");
                bSignature.initVerify(secondPartyPublicKey);
                bSignature.update(CryptMath.toByteArr(g_y,g_x));
                if(!bSignature.verify(CryptMath.toByteArr(sig_a))){
                    System.out.println("Failed to verify signature of g_y, and g_x values");
                    return;
                }

                if(!mac_k_m_a.equals(
                        new BigInteger(mac.doFinal(CryptMath.toByteArr(a_modulus, a_exponent, sig_a))))){
                    System.out.println("Failed to verify 3 packet mac.");
                }

                System.out.println("received 3 packet");
            }
        } catch (NoSuchAlgorithmException | InvalidKeyException | SignatureException | InvalidKeySpecException ex){
            ex.printStackTrace();
        } catch (InterruptedException e) {}
    }

    public SecretKey getKey(){
        return encryptionKey;
    }


    private void prepareTransmissionEncryption(BigInteger initializer) throws NoSuchAlgorithmException, InvalidKeySpecException{
        SecretKeyFactory fac=SecretKeyFactory.getInstance("PBKDF2WithHmacSHA256");
        KeySpec spec=new PBEKeySpec(initializer.toString().toCharArray(), "salty_transmission_encryption".getBytes(), 10, 256);

        transmissionKey=new SecretKeySpec(fac.generateSecret(spec).getEncoded(), "AES");

        try {
            transmissionEncryptionCipher=Cipher.getInstance("AES");
            transmissionEncryptionCipher.init(Cipher.ENCRYPT_MODE, transmissionKey);

            transmissionDecryptionCipher=Cipher.getInstance("AES");
            transmissionDecryptionCipher.init(Cipher.DECRYPT_MODE, transmissionKey);
        } catch (NoSuchPaddingException | InvalidKeyException e) {
            e.printStackTrace();
        }
    }
    private void prepareEncryption(BigInteger initializer) throws NoSuchAlgorithmException, InvalidKeySpecException{
        SecretKeyFactory fac=SecretKeyFactory.getInstance("PBKDF2WithHmacSHA256");
        KeySpec spec=new PBEKeySpec(initializer.toString().toCharArray(), "salty_encryption".getBytes(), 10, 256);

        encryptionKey=new SecretKeySpec(fac.generateSecret(spec).getEncoded(), "AES");

        try {
            encryptionCipher=Cipher.getInstance("AES");
            encryptionCipher.init(Cipher.ENCRYPT_MODE, encryptionKey);

            decryptionCipher=Cipher.getInstance("AES");
            decryptionCipher.init(Cipher.DECRYPT_MODE, encryptionKey);
        } catch (NoSuchPaddingException | InvalidKeyException e) {
            e.printStackTrace();
        }
    }
    private void prepareMac(BigInteger initializer) throws NoSuchAlgorithmException, InvalidKeySpecException{
        SecretKeyFactory fac=SecretKeyFactory.getInstance("PBKDF2WithHmacSHA256");
        KeySpec spec=new PBEKeySpec(initializer.toString().toCharArray(), "salty_mac".getBytes(), 11, 256);

        macKey=new SecretKeySpec(fac.generateSecret(spec).getEncoded(), "HmacSHA256");

        try {
            mac=Mac.getInstance("HmacSHA256");
            mac.init(macKey);
        } catch (InvalidKeyException e) {
            e.printStackTrace();
        }
    }

    private BigInteger encrypt(Cipher cipher, BigInteger... data){
        try {
            return new BigInteger(cipher.doFinal(CryptMath.pack(data)));
        } catch (IllegalBlockSizeException | BadPaddingException e) {
            e.printStackTrace();
        }

        return null;
    }

    private BigInteger[] decrypt(Cipher cipher, BigInteger data){
        try {
            return CryptMath.unpack(cipher.doFinal(data.toByteArray()));
        } catch (IllegalBlockSizeException | BadPaddingException e) {
            e.printStackTrace();
        }
        return null;
    }

    @Override
    public void close(){
        inChannel.disconnect(receiver);
    }
}