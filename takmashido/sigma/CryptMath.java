package takmashido.sigma;

import java.math.BigInteger;
import java.util.ArrayList;
import java.util.List;

public class CryptMath{
    public static byte[] toByteArr(BigInteger... dat){
        ArrayList<Byte[]> bytes=new ArrayList<>(dat.length);
        int length=0;
        for(int i=0;i<dat.length;i++){
            byte[] b=dat[i].toByteArray();
            bytes.add(byteToByte(b));
            length+=b.length;
        }

        byte[] ret=new byte[length];
        int index=0;
        for(Byte[] byt: bytes){
            for(int i=0;i<byt.length;i++)
                ret[index++]=byt[i];
        }
        return ret;
    }
    public static Byte[] byteToByte(byte... bytes){
        Byte[] ret=new Byte[bytes.length];
        for(int i=0;i<bytes.length;i++)
            ret[i]=bytes[i];
        return ret;
    }
    
    public static byte[] pack(BigInteger... data){
        ArrayList<Byte> ret=new ArrayList<>();

        for(byte byt:intToByteArray(data.length))
            ret.add(byt);

        for(BigInteger dat:data){
            byte[] rawDat=dat.toByteArray();
            for(byte byt:intToByteArray(rawDat.length))
                ret.add(byt);

            for(byte byt:rawDat)
                ret.add(byt);
        }

        System.out.println("packlength: "+getBytes(ret).length);
        return getBytes(ret);
    }
    private static byte[] intToByteArray( int data ) {    
        byte[] result = new byte[4];
        result[0] = (byte) ((data & 0xFF000000) >> 24);
        result[1] = (byte) ((data & 0x00FF0000) >> 16);
        result[2] = (byte) ((data & 0x0000FF00) >> 8);
        result[3] = (byte) ((data & 0x000000FF) >> 0);
        return result;        
    }
    private static byte[] getBytes(List<Byte> data){
        byte[] ret=new byte[data.size()];
        int i=0;
        for(byte byt:data)
            ret[i++]=byt;
        return ret;
    }

    public static BigInteger[] unpack(byte[] data){
        int length=byteToInt(data, 0);

        int offset=4;
        BigInteger[] ret=new BigInteger[length];
        for(int i=0;i<length;i++){
            int len=byteToInt(data, offset);
            offset+=4;
            ret[i]=new BigInteger(data, offset, len);
            offset+=len;
        }

        return ret;
    }
    private static int byteToInt(byte[] data, int offset){
        int ret=0;
        ret+=data[offset+0]<<24;
        ret+=data[offset+1]<<16;
        ret+=data[offset+2]<<8;
        ret+=data[offset+3];
        return ret;
    }
}