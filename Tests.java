import java.math.BigInteger;
import java.security.NoSuchAlgorithmException;
import java.util.Base64;

import takmashido.channel.BasicChannel;
import takmashido.channel.Channel;
import takmashido.sigma.CryptMath;
import takmashido.sigma.SigmaAgent;

public final class Tests{
    public static void main(String[]args) throws NoSuchAlgorithmException{
        // packUnpackTest();
        agentTest();
    }

    public static void packUnpackTest(){
        BigInteger a=BigInteger.valueOf(131231);
        BigInteger b=BigInteger.valueOf(8712365011l);
        BigInteger c=a.pow(15);
        BigInteger d=b.modPow(a, c);

        BigInteger[] result=CryptMath.unpack(CryptMath.pack(a,b,c,d));
        if(!result[0].equals(a)||
           !result[1].equals(b)||
           !result[2].equals(c)||
           !result[3].equals(d))
            System.out.println("Failed 0");
        

        BigInteger e=BigInteger.valueOf(0xffffffffffffffffl);
        result=CryptMath.unpack(CryptMath.pack(c,e,d,a));
        if(!result[0].equals(c)||
           !result[1].equals(e)||
           !result[2].equals(d)||
           !result[3].equals(a))
            System.out.println("Failed 1");

        System.out.println("Packing test finished");
    }

    public static void agentTest() throws NoSuchAlgorithmException{
        Channel channel1=new BasicChannel();
        Channel channel2=new BasicChannel();
        SigmaAgent first=new SigmaAgent(channel1, channel2, true);
        SigmaAgent second=new SigmaAgent(channel2, channel1, false);

        Thread firstThread=new Thread(first);
        Thread secondThread=new Thread(second);

        firstThread.start();
        secondThread.start();

        try {
            firstThread.join();
            secondThread.join();
        } catch (InterruptedException e) {}

        if(!first.getKey().equals(second.getKey()))
            System.out.println("test failed");

        System.out.println("First agent key:  "+Base64.getEncoder().encodeToString(first.getKey().getEncoded()));
        System.out.println("Second agent key: "+Base64.getEncoder().encodeToString(second.getKey().getEncoded()));
        
        System.out.println("Sigma agent test finished");
    }
}